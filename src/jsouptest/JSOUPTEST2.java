/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsouptest;

import java.io.IOException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import static jsouptest.JSOUPTEST.getHttpHeaders;
import static jsouptest.JSOUPTEST.trustEveryone;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 *
 * @author IanChien
 */
public class JSOUPTEST2 {
    public static void trustEveryone() {  
        try {  
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {  
                public boolean verify(String hostname, SSLSession session) {  
                    return true;  
                }  
            });  
  
            SSLContext context = SSLContext.getInstance("TLS");  
            context.init(null, new X509TrustManager[] { new X509TrustManager() {  
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {  
                }  
  
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {  
                }  
  
                public X509Certificate[] getAcceptedIssuers() {  
                    return new X509Certificate[0];  
                }  
            } }, new SecureRandom());  
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());  
        } catch (Exception e) {  
            // e.printStackTrace();  
        }  
    }
    
    
    public static Object getHttpHeaders(URL url, int timeout) {  
        try {  
            trustEveryone();  
            Connection conn = HttpConnection.connect(url);  
            conn.timeout(timeout);  
            conn.header("Accept-Encoding", "gzip,deflate,sdch");  
            conn.header("Connection", "close");  
            conn.get();  
            Map<String, String> result = conn.response().headers();  
            result.put("title", conn.response().parse().title());  
            return result;  
  
        } catch (Exception e) {  
            //e.printStackTrace();  
        }  
        return null;  
    }  
    
     public static void main(String[] args)  {
         String username="zzz@hotmial.cpm";
         String password="zzzzzzz";
         
        try {
 //trustEveryone(); 
             String url = "https://www.linkedin.com/uas/login?goback=&trk=hb_signin";
             Connection.Response response = Jsoup
                     .connect(url)
                     .method(Connection.Method.GET)
                     .execute();

             Document responseDocument = response.parse();
             Element loginCsrfParam = responseDocument
                     .select("input[name=loginCsrfParam]")
                     .first();

             response = Jsoup.connect("https://www.linkedin.com/uas/login-submit")
                     .cookies(response.cookies())
                     .data("loginCsrfParam", loginCsrfParam.attr("value"))
                     .data("session_key", username)
                     .data("session_password", password)
                     .method(Connection.Method.POST)
                     .followRedirects(true)
                     .execute();

             Document document = response.parse();

             System.out.println("Welcome " 
                     + document.select(".act-set-name-split-link").html());


        
            
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
}
